using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class SimpleAnchor : MonoBehaviour
{

    public GameObject visualization;

    ARRaycastManager m_RaycastManager;
    ARAnchorManager m_AnchorManager;

    ARAnchor previousAnchor;
    static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();

    // Start is called before the first frame update
    void Start()
    {
        visualization.SetActive(false);
        m_RaycastManager = GetComponent<ARRaycastManager>();
        m_AnchorManager = GetComponent<ARAnchorManager>();

    }


    ARAnchor CreateAnchor(in ARRaycastHit hit)
    {
        ARAnchor anchor = null;

        // If we hit a plane, try to "attach" the anchor to the plane
        if (hit.trackable is ARPlane plane)
        {
            var planeManager = GetComponent<ARPlaneManager>();
            if (planeManager)
            {
                Debug.Log("SAE: Creating plane anchor.");
                anchor = m_AnchorManager.AttachAnchor(plane, hit.pose);
                return anchor;
            }
        }

        // Otherwise, just create a regular anchor at the hit pose
        Debug.Log("SAE: Creating regular anchor.");

        // Note: the anchor can be anywhere in the scene hierarchy
        var gameObject = new GameObject();
        gameObject.transform.position = hit.pose.position;
        gameObject.transform.rotation = hit.pose.rotation;
        anchor = gameObject.AddComponent<ARAnchor>();

        return anchor;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount == 0)
            return;

        var touch = Input.GetTouch(0);
        if (touch.phase != TouchPhase.Began)
            return;

        // Raycast against planes and feature points
        const TrackableType trackableTypes =
            TrackableType.FeaturePoint |
            TrackableType.PlaneWithinPolygon;

        // Perform the raycast
        if (m_RaycastManager.Raycast(touch.position, s_Hits, trackableTypes))
        {
            // Raycast hits are sorted by distance, so the first one will be the closest hit.
            var hit = s_Hits[0];

            // Create a new anchor
            var anchor = CreateAnchor(hit);
            if (anchor)
            {
                var planeManager = GetComponent<ARPlaneManager>();
                if (planeManager)
                {
                    planeManager.SetTrackablesActive(false);
                }

                visualization.SetActive(true);
                visualization.transform.parent = anchor.transform;
                visualization.transform.localPosition = Vector3.zero;
                visualization.transform.localRotation = Quaternion.Euler(0, 180, 0);

                if (previousAnchor)
                {
                    Destroy(previousAnchor.gameObject);
                }
                // Remember the anchor so we can remove it later.
                previousAnchor = anchor;
            }
            else
            {
                Debug.LogWarning("SAE: Error creating anchor");
            }
        }
    }
}
